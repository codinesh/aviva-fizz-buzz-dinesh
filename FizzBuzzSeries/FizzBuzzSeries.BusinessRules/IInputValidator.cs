﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public interface IDayFinder
    {
        DayOfWeek DayOfWeek { get; }    
    }
}
