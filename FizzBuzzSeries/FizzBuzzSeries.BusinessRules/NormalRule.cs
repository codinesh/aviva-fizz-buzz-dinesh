﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public class NormalRule : INumberRule
    {
        public int Number { get; set; }

        public bool CanProcess()
        {
            return (Number > 0);
        }

        public string DisplayText
        {
            get { return CanProcess() ? Number.ToString() : null; }
        }
    }
}
