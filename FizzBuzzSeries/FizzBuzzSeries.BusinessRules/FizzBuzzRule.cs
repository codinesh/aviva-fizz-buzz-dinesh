﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public class FizzBuzzRule : INumberRule
    {
        IDayFinder dayFinder;
        public FizzBuzzRule(IDayFinder dayFinder)
        {
            this.dayFinder = dayFinder;
        }

        public int Number { get; set; }

        public bool CanProcess()
        {
            return (Number > 0 && Number % 15 == 0);
        }

        public string DisplayText
        {
            get
            {
                string value = null;
                if (CanProcess())
                {
                    value = (dayFinder.DayOfWeek == DayOfWeek.Wednesday) ? ("wizz wuzz") : ("fizz buzz");
                }
                return value;
            }
        }
    }
}
