﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public class InputRangeValidator : IInputValidator
    {
        public bool IsValid(int number)
        {
            return (number >= 1 && number <= 1000);
        }
    }
}
