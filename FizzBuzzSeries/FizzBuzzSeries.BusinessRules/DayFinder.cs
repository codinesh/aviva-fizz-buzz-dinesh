﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public class DayFinder : IDayFinder
    {
        public DayOfWeek DayOfWeek
        {
            get
            {
                return DateTime.Now.DayOfWeek;
            }
        }
    }
}
