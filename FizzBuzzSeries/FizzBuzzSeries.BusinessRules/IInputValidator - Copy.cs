﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public interface IInputValidator
    {
        /// <summary>
        /// return a boolean value indicating whether the current instance validates 
        /// the provided number or not.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        bool IsValid(int number);
    }
}
