﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public class BuzzRule : INumberRule
    {
        IDayFinder dayFinder;

        public BuzzRule(IDayFinder dayFinder)
        {
            this.dayFinder = dayFinder;
        }

        public int Number { get; set; }

        public bool CanProcess()
        {
            return (Number > 0 && Number % 3 != 0 && Number % 5 == 0);
        }

        public string DisplayText
        {
            get
            {
                string value = null;
                if (CanProcess())
                {
                    value = (dayFinder.DayOfWeek == DayOfWeek.Wednesday) ? ("wuzz") : ("buzz");
                }
                return value;
            }
        }
    }
}
