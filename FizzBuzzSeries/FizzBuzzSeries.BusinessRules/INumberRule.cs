﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules
{
    public interface INumberRule
    {
        /// <summary>
        /// Number based on which the rules will be applied.
        /// </summary>
        int Number { get; set; }

        /// <summary>
        /// Returns a boolean value indicating whether the instance can process this 
        /// current number or not.
        /// </summary>
        /// <returns></returns>
        bool CanProcess();

        /// <summary>
        /// Displays the appropriate text to be displayed for the value in Number.
        /// </summary>
        string DisplayText { get; }
    }
}
