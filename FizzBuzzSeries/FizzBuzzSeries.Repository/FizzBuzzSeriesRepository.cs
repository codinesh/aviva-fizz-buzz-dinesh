﻿using FizzBuzzSeries.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzzSeries.Repository
{
    public class FizzBuzzSeriesRepository : IRepository
    {
        public virtual IEnumerable<INumberRule> businessRules { get; set; }
        public FizzBuzzSeriesRepository()
        {

        }
        public FizzBuzzSeriesRepository(IEnumerable<INumberRule> rules)
        {
            businessRules = rules;
        }

        public virtual IList<string> GenerateSeries(int maxNumber)
        {
            IList<string> outputSeries = new List<string>();

            for (int count = 1; count <= maxNumber; count++)
            {
                //var businessRulesList = businessRules.ToList();
                //businessRulesList.ForEach(rule => rule.Number = count);

                // Initialize the value of number in every available instance of INumberRule
                foreach (var number in businessRules)
                {
                    number.Number = count;
                }

                // Find the instance which closely matches the business criteria for the current number.
                var matchingRule = businessRules.FirstOrDefault<INumberRule>(rule => rule.CanProcess());
                if (matchingRule != null)
                {
                    outputSeries.Add(matchingRule.DisplayText);
                }
            }

            return outputSeries;
        }
    }
}