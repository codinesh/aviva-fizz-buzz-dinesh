﻿using FizzBuzzSeries.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzzSeries.Repository
{
    public class FizzBuzzSeriesRepositoryUpgraded : FizzBuzzSeriesRepository, IRepositoryUpgradedVersion
    {
        public FizzBuzzSeriesRepositoryUpgraded(IEnumerable<INumberRule> rules)
        {
            base.businessRules = rules;
        }
        
        public IList<string> GenerateSeries(int startNumber, int count, int maxNumber)
        {
            IList<string> outputSeries = new List<string>();
            var endNumber = startNumber + count;
            for (int number = startNumber; number < endNumber && number <= maxNumber; number++)
            {
                //var businessRulesList = businessRules.ToList();
                //businessRulesList.ForEach(rule => rule.Number = count);

                // Initialize the value of number in every available instance of INumberRule
                foreach (var rule in businessRules)
                {
                    rule.Number = number;
                }

                // Find the instance which closely matches the business criteria for the current number.
                var matchingRule = businessRules.FirstOrDefault<INumberRule>(rule => rule.CanProcess());
                if (matchingRule != null)
                {
                    outputSeries.Add(matchingRule.DisplayText);
                }
            }

            return outputSeries;
        }
    }
}