﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.Repository
{
    public interface IRepositoryUpgradedVersion : IRepository
    {
        /// <summary>
        /// Returns a collection of strings with appropriate display values.
        /// </summary>
        /// <param name="MaxNumber"></param>
        /// <returns></returns>
        IList<string> GenerateSeries(int startNumber, int count, int maxNumber);
    }
}
