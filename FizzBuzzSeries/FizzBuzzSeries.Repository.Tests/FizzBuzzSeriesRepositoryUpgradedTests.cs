﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzSeries.Repository;
using NUnit.Framework;
using StructureMap;
using FizzBuzzSeries.BusinessRules;
using Moq;

namespace FizzBuzzSeries.Repository.Tests
{
    [TestFixture]
    public class FizzBuzzSeriesRepositoryUpgradedTests
    {
        #region properties
        private IRepositoryUpgradedVersion repository;

        private Mock<INumberRule> numberRuleMock;
        private IEnumerable<INumberRule> numberRules;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            RegisterAbstractToConcreteTypes();
            repository = new FizzBuzzSeriesRepositoryUpgraded(numberRules);
        }

        /// <summary>
        /// Register mapping between Abstract types and concrete types 
        /// so that structure map can inject the required dependencies.
        /// </summary>
        protected void RegisterAbstractToConcreteTypes()
        {
            var listOfNumberMock = new List<INumberRule>();

            numberRuleMock = new Mock<INumberRule>();
            numberRuleMock.Setup(rule => rule.Number).Returns(1);
            numberRuleMock.Setup(rule => rule.CanProcess()).Returns(true);
            numberRuleMock.Setup(rule => rule.DisplayText).Returns("1");
            listOfNumberMock.Add(numberRuleMock.Object);

            numberRules = listOfNumberMock;

            ObjectFactory.Initialize(type =>
            {
                type.For<INumberRule>().Use(numberRuleMock.Object);
                type.For<IEnumerable<INumberRule>>().Use(numberRules);
            });
        }
        #endregion

        #region Test Methods
        [Test]
        public void GenerateSeries_Method_Should_Return_a_List_of_Strings()
        {
            var series = repository.GenerateSeries(12);
            Assert.IsInstanceOf<IList<string>>(series);
        }

        [Test]
        public void GenerateSeries_Method_Should_return_list_if_1_is_passed()
        {
            var series = repository.GenerateSeries(1);
            Assert.AreEqual(1, series.Count);
        }

        [Test]
        public void GenerateSeries_Method_Should_return_list_if_1000_is_passed()
        {
            var series = repository.GenerateSeries(1000);
            Assert.AreEqual(1000, series.Count);
        }

        [Test]
        public void GenereateSeries_method_should_take_count_as_parameter_and_return_list_of_size_count() {
            var series = repository.GenerateSeries(1, 20, 1000);
            Assert.AreEqual(20, series.Count);
        }
        #endregion
    }
}
