﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Web.Mvc;
using FizzBuzzSeries.Web.Controllers;
using Moq;
using FizzBuzzSeries.Web;
using FizzBuzzSeries.Web.Models;
using FizzBuzzSeries.Repository;
using FizzBuzzSeries.BusinessRules;

namespace FizzBuzzSeries.Web.Tests
{
    [TestFixture]
    class HomeControllerTests
    {
        #region private members
        private HomeController homeController;
        private Mock<SeriesViewModel> mockSeriesViewModel;
        Mock<IRepositoryUpgradedVersion> mockRepository;
        Mock<IInputValidator> mockInputValidator;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            mockRepository = new Mock<IRepositoryUpgradedVersion>();
            mockRepository.Setup(repo => repo.GenerateSeries(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(
                    new List<string> { 
                    "1","2","fizz","4","buzz","fizz","7","8","fizz","10","11", "12", "13",
                    "14","fizz buzz", "16", "17", "fizz", "19", "buzz"
                    });

            mockInputValidator = new Mock<IInputValidator>();
            mockInputValidator.Setup(result => result.IsValid(It.IsAny<int>())).Returns((int x) => { return (x > 1 && x < 1000); });

            homeController = new HomeController(mockRepository.Object,mockInputValidator.Object);
            mockSeriesViewModel = new Mock<SeriesViewModel>();

            mockSeriesViewModel.Setup(viewModel => viewModel.MaxNumber).Returns(() => 30);
            mockSeriesViewModel.Setup(viewModel => viewModel.StartNumber).Returns(() => 1);
            mockSeriesViewModel.Setup(viewModel => viewModel.NumbersPerPage).Returns(() => 20);
        }
        #endregion

        #region Test Methods
        [Test]
        public void Series_ActionMethod_Should_Return_A_View()
        {
            var result = homeController.Series();
            Assert.IsInstanceOf<ViewResult>(result);
        }

        [Test]
        public void DisplaySeries_ActionMethod_Should_Accept_SeriesViewModel_With_Number_And_Return_View()
        {
            var result = homeController.DisplaySeries(mockSeriesViewModel.Object);
            Assert.IsInstanceOf<ViewResult>(result);
        }

        [Test]
        public void DisplaySeries_ActionMethod_Should_Have_a_Model_With_List()
        {
            var result = homeController.DisplaySeries(mockSeriesViewModel.Object);
            Assert.IsInstanceOf<List<string>>((result.Model as SeriesViewModel).FizzBuzzSeries);
        }

        [Test]
        public void Generated_Series_Should_generate_list_of_Count_specified()
        {
            var result = homeController.DisplaySeries(mockSeriesViewModel.Object);
            Assert.AreEqual(20, (result.Model as SeriesViewModel).FizzBuzzSeries.Count);
        }

        [Test]
        [ExpectedException]
        public void DisplaySeries_should_throw_error_if_input_is_out_of_range()
        {
            var mockRepository = new Mock<IRepositoryUpgradedVersion>();
            mockRepository.Setup(repo => repo.GenerateSeries(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(
                               new List<string> { 
                    "1","2","fizz","4","buzz","fizz","7","8","fizz","10","11", "12", "13",
                    "14","fizz buzz", "16", "17", "fizz", "19", "buzz"
                    });

            var mockInputValidator = new Mock<IInputValidator>();
            mockInputValidator.Setup(result => result.IsValid(It.IsAny<int>())).Returns((int x) => { return (x > 1 && x < 1000); });

            homeController = new HomeController(mockRepository.Object, mockInputValidator.Object);
            var mockSeriesViewModel = new Mock<SeriesViewModel>();

            mockSeriesViewModel.Setup(viewModel => viewModel.MaxNumber).Returns(() => 0);
            mockSeriesViewModel.Setup(viewModel => viewModel.StartNumber).Returns(() => 1);
            mockSeriesViewModel.Setup(viewModel => viewModel.NumbersPerPage).Returns(() => 20);

            homeController.DisplaySeries(mockSeriesViewModel.Object);
        }
        #endregion
    }
}
