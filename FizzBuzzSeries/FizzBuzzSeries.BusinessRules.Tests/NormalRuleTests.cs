﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules.Tests
{
    [TestFixture]
    public class NormalRuleTests
    {
        #region private members
        private INumberRule normalRule;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            normalRule = new NormalRule();
        }
        #endregion

        #region Test Methods
        [Test]
        public void CanProcess_Method_Should_return_True_When_One_is_Passed()
        {
            normalRule.Number = 1;
            Assert.AreEqual(true, normalRule.CanProcess());
        }

        [Test]
        public void CanProcess_Method_Should_return_the_number_passed_as_string()
        {
            normalRule.Number = 1;
            Assert.AreEqual("1", normalRule.DisplayText);
        }


        [Test]
        public void CanProcess_Method_Should_return_False_When_Negative_number_is_Passed()
        {
            normalRule.Number = -1;
            Assert.AreEqual(false, normalRule.CanProcess());
        }

        [Test]
        public void CanProcess_Method_Should_return_null_if_negative_number_is_passed()
        {
            normalRule.Number = -1;
            Assert.AreEqual(null, normalRule.DisplayText);
        }
        #endregion
    }
}
