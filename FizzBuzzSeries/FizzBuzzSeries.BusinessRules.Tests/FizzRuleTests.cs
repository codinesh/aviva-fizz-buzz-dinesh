﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules.Tests
{
    [TestFixture]
    public class FizzRuleTests
    {
        #region private members
        private INumberRule fizzRule;
        private Mock<IDayFinder> mockDayFinder;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            mockDayFinder = new Mock<IDayFinder>();
            mockDayFinder.Setup(rule => rule.DayOfWeek).Returns(DayOfWeek.Monday);
            fizzRule = new FizzRule(mockDayFinder.Object);
        }
        #endregion

        #region Test Methods
        [Test]
        public void CanProcess_Method_Should_return_True_When_3_is_Passed()
        {
            fizzRule.Number = 3;
            Assert.AreEqual(true, fizzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_Should_return_fizz()
        {
            fizzRule.Number = 3;
            Assert.AreEqual("fizz", fizzRule.DisplayText);
        }


        [Test]
        public void CanProcess_Method_Should_return_False_When_Negative_number_is_Passed()
        {
            fizzRule.Number = -1;
            Assert.AreEqual(false, fizzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_null_if_negative_number_is_passed()
        {
            fizzRule.Number = -1;
            Assert.AreEqual(null, fizzRule.DisplayText);
        }

        [Test]
        public void CanProcess_Method_Should_return_False_When_2_is_Passed()
        {
            fizzRule.Number = -1;
            Assert.AreEqual(false, fizzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_null_if_2_is_passed()
        {
            fizzRule.Number = -1;
            Assert.AreEqual(null, fizzRule.DisplayText);
        }

        [Test]
        public void DisplayText_Property_should_return_wizz_on_Wednesday()
        {
            var mockDayFinder = new Mock<IDayFinder>();
            mockDayFinder.Setup(rule => rule.DayOfWeek).Returns(DayOfWeek.Wednesday);
            var fizzRule = new FizzRule(mockDayFinder.Object);
            fizzRule.Number = 6;
            Assert.AreEqual("wizz", fizzRule.DisplayText);
        }
        #endregion
    }
}
