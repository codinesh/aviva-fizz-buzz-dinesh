﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules.Tests
{
    [TestFixture]
    public class FizzBuzzRuleTests
    {
        #region private members
        private INumberRule fizzbuzzRule;
        private Mock<IDayFinder> mockDayFinder;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            mockDayFinder = new Mock<IDayFinder>();
            mockDayFinder.Setup(rule => rule.DayOfWeek).Returns(DayOfWeek.Monday);
            fizzbuzzRule = new FizzBuzzRule(mockDayFinder.Object);
        }
        #endregion

        #region Test Methods

        [Test]
        public void CanProcess_Method_Should_return_True_When_15_is_Passed()
        {
            fizzbuzzRule.Number = 15;
            Assert.AreEqual(true, fizzbuzzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_Should_return_fizzbuzz()
        {
            fizzbuzzRule.Number = 15;
            Assert.AreEqual("fizz buzz", fizzbuzzRule.DisplayText);
        }

        [Test]
        public void CanProcess_Method_Should_return_False_When_Negative_number_is_Passed()
        {
            fizzbuzzRule.Number = -1;
            Assert.AreEqual(false, fizzbuzzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_null_if_negative_number_is_passed()
        {
            fizzbuzzRule.Number = -1;
            Assert.AreEqual(null, fizzbuzzRule.DisplayText);
        }

        [Test]
        public void CanProcess_Method_Should_return_False_When_5_is_Passed()
        {
            fizzbuzzRule.Number = -1;
            Assert.AreEqual(false, fizzbuzzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_null_if_5_is_passed()
        {
            fizzbuzzRule.Number = -1;
            Assert.AreEqual(null, fizzbuzzRule.DisplayText);
        }

        [Test]
        public void DisplayText_Property_should_return_wizzwuzz_on_Wednesday()
        {
            var mockDayFinder = new Mock<IDayFinder>();
            mockDayFinder.Setup(rule => rule.DayOfWeek).Returns(DayOfWeek.Wednesday);
            var fizzbuzzRule = new FizzBuzzRule(mockDayFinder.Object);
            fizzbuzzRule.Number = 15;
            Assert.AreEqual("wizz wuzz", fizzbuzzRule.DisplayText);
        }
        #endregion
    }
}
