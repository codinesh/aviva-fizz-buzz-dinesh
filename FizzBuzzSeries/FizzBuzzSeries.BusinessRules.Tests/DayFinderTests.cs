﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules.Tests
{

    [TestFixture]
    public class DayFinderTests
    {
        #region private members
        private IDayFinder dayFinder;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            dayFinder = new DayFinder();
        }
        #endregion

        #region Test Methods
        [Test]
        public void DayOfWeek_should_return_dayOfWeek_property()
        {
            Assert.IsInstanceOf<DayOfWeek>(dayFinder.DayOfWeek);
        }

        #endregion
    }
}
