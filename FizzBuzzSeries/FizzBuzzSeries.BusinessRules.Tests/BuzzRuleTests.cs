﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules.Tests
{

    [TestFixture]
    public class BuzzRuleTests
    {
        #region private members
        private INumberRule buzzRule;
        private Mock<IDayFinder> mockDayFinder;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            mockDayFinder = new Mock<IDayFinder>();
            mockDayFinder.Setup(rule => rule.DayOfWeek).Returns(DayOfWeek.Monday);
            buzzRule = new BuzzRule(mockDayFinder.Object);
        }
        #endregion

        #region Test Methods
        [Test]
        public void CanProcess_Method_Should_return_True_When_5_is_Passed()
        {
            buzzRule.Number = 5;
            Assert.AreEqual(true, buzzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_Should_return_fizz()
        {
            buzzRule.Number = 5;
            Assert.AreEqual("buzz", buzzRule.DisplayText);
        }


        [Test]
        public void CanProcess_Method_Should_return_False_When_Negative_number_is_Passed()
        {
            buzzRule.Number = -1;
            Assert.AreEqual(false, buzzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_null_if_negative_number_is_passed()
        {
            buzzRule.Number = -1;
            Assert.AreEqual(null, buzzRule.DisplayText);
        }

        [Test]
        public void CanProcess_Method_Should_return_False_When_6_is_Passed()
        {
            buzzRule.Number = -1;
            Assert.AreEqual(false, buzzRule.CanProcess());
        }

        [Test]
        public void DisplayText_Property_null_if_6_is_passed()
        {
            buzzRule.Number = -1;
            Assert.AreEqual(null, buzzRule.DisplayText);
        }

        [Test]
        public void DisplayText_Property_should_return_wuzz_on_Wednesday()
        {
            var mockDayFinder = new Mock<IDayFinder>();
            mockDayFinder.Setup(rule => rule.DayOfWeek).Returns(DayOfWeek.Wednesday);
            var buzzRule = new BuzzRule(mockDayFinder.Object);
            buzzRule.Number = 10;
            Assert.AreEqual("wuzz", buzzRule.DisplayText);
        }
        #endregion
    }
}
