﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzSeries.BusinessRules.Tests
{
    [TestFixture]
    public class InputValidatorTests
    {
        #region private members
        private IInputValidator inputRangeValidator;
        #endregion

        #region Test Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            inputRangeValidator = new InputRangeValidator();
        }
        #endregion

        #region Test Methods
        [Test]
        public void IsValid_should_return_true_if_value_is_20()
        {
            var result = inputRangeValidator.IsValid(20);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void IsValid_should_return_true_if_value_is_1()
        {
            var result = inputRangeValidator.IsValid(1);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void IsValid_should_return_true_if_value_is_1000()
        {
            var result = inputRangeValidator.IsValid(1000);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void IsValid_should_return_false_if_value_is_0()
        {
            var result = inputRangeValidator.IsValid(0);
            Assert.AreEqual(false, result);
        }

        [Test]
        public void IsValid_should_return_false_if_value_is_Negative5()
        {
            var result = inputRangeValidator.IsValid(-5);
            Assert.AreEqual(false, result);
        }
        #endregion
    }
}
