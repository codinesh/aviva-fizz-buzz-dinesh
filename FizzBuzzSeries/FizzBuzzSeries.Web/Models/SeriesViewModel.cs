﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FizzBuzzSeries.Web.Models
{
    public class SeriesViewModel
    {
        [Required]
        [DisplayName("Maximum Number")]
        [Range(1, 1000, ErrorMessage = "Number should be between 1 and 1000")]
        public virtual int MaxNumber { get; set; }
        public IList<string> FizzBuzzSeries { get; set; }

        public virtual int StartNumber { get; set; }
        public virtual int NumbersPerPage { get { return 20; } }

        public bool GoPrevious { get; set; }
        public bool GoNext { get; set; }
    }
}
