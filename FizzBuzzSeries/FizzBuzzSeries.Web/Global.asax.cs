﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FizzBuzzSeries.Web.App_Data;
using StructureMap;
using FizzBuzzSeries.Repository;
using FizzBuzzSeries.BusinessRules;

namespace FizzBuzzSeries.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            this.RegisterAbstractToConcreteTypes();
            this.SetResolver();
            ControllerBuilder.Current.SetControllerFactory(new StructureMapController());
        }

        /// <summary>
        /// Register mapping between Abstract types and concrete types 
        /// so that structure map can inject the required dependencies.
        /// </summary>
        protected void RegisterAbstractToConcreteTypes()
        {
            ObjectFactory.Initialize(type =>
            {
                type.For<IRepositoryUpgradedVersion>().Use<FizzBuzzSeriesRepositoryUpgraded>();          
                type.For<IDayFinder>().Use<DayFinder>();
                type.For<IInputValidator>().Use<InputRangeValidator>();    
                type.For<INumberRule>().Use<FizzBuzzRule>();
                type.For<INumberRule>().Use<BuzzRule>();
                type.For<INumberRule>().Use<FizzRule>();
                type.For<INumberRule>().Use<NormalRule>();
                type.For<IRepository>().Use<FizzBuzzSeriesRepository>();                
            });
        }

        /// <summary>
        /// Resolve the instance conflict between MVC and Structure Map.
        /// </summary>
        private void SetResolver()
        {
            DependencyResolver.SetResolver(
                t =>
                {
                    try
                    {
                        return ObjectFactory.GetInstance(t);
                    }
                    catch
                    {
                        return null;
                    }
                },
                t => ObjectFactory.GetAllInstances<object>().Where(s => s.GetType() == t));
        }
    }
}
