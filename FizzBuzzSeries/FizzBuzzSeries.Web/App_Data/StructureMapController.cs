﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace FizzBuzzSeries.Web.App_Data
{
    public class StructureMapController : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType != null)
            {
                // Attempt to use the Object Factory to create an instance of the Controller
                return ObjectFactory.GetInstance(controllerType) as Controller;
            }
            else
            {
                // If we get any type of exception then attempt to use the base factory
                return base.GetControllerInstance(requestContext, controllerType);
            }
        }
    }
}
