﻿using FizzBuzzSeries.BusinessRules;
using FizzBuzzSeries.Repository;
using FizzBuzzSeries.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzzSeries.Web.Controllers
{
    public class HomeController : Controller
    {
        #region private members
        IRepositoryUpgradedVersion fizzbuzzSeriesRepository;
        public IInputValidator fizzBuzzSeriesInputValidator;
        #endregion

        /// <summary>
        /// Inject instances of IRepository and IInputValidator to the constructor  
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="inputValidations"></param>
        public HomeController(IRepositoryUpgradedVersion repository, IInputValidator inputValidations)
        {
            this.fizzbuzzSeriesRepository = repository;
            this.fizzBuzzSeriesInputValidator = inputValidations;
        }

        /// <summary>
        /// This action method returns the basic view with textbox input 
        /// to accept a number.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Series()
        {
            return View();
        }

        /// <summary>
        /// This method will be called by form submit post action, and 
        /// returns fizz-buzz series.
        /// </summary>
        /// <param name="seriesViewModel"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult DisplaySeries(SeriesViewModel seriesViewModel)
        {
            var validationResult = fizzBuzzSeriesInputValidator.IsValid(seriesViewModel.MaxNumber);
            if (ModelState.IsValid && !validationResult)
            {
                throw new Exception("Input value is out of range.");
            }

            //commented as repository upgraded to next version.
            //seriesViewModel.FizzBuzzSeries = fizzbuzzSeriesRepository.GenerateSeries(seriesViewModel.MaxNumber);
            if (seriesViewModel.GoPrevious)
            {
                seriesViewModel.StartNumber = ((seriesViewModel.StartNumber - seriesViewModel.NumbersPerPage) > 0) ? (seriesViewModel.StartNumber - seriesViewModel.NumbersPerPage) : (1);
            }
            else if (seriesViewModel.GoNext)
            {
                seriesViewModel.StartNumber = ((seriesViewModel.StartNumber + seriesViewModel.NumbersPerPage) <= seriesViewModel.MaxNumber) ? (seriesViewModel.StartNumber + seriesViewModel.NumbersPerPage) : (seriesViewModel.MaxNumber);
            }

            seriesViewModel.FizzBuzzSeries = fizzbuzzSeriesRepository.GenerateSeries(seriesViewModel.StartNumber, seriesViewModel.NumbersPerPage, seriesViewModel.MaxNumber);
            return View("Series", seriesViewModel);
        }
    }
}